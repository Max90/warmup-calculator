package de.feigl.warmupcalculator;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {
    private EditText startingWeight;
    private EditText endingWeight;
    private TextView increment;
    private TextView firstSet;
    private TextView secondSet;
    private TextView thirdSet;
    private TextView forthSet;
    private TextView fifthSet;
    ArrayList plates;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instantiateWeights();
        increment = (TextView) findViewById(R.id.tv_increment);
        instantiateSetStrings();

        plates = new ArrayList<Float>();
        setPlates();
        setIncrementText();
        calculateFirstTwoSets();
        startingWeight.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                setIncrementText();
                calculateFirstTwoSets();
                calculateThirdSet();
                calculateThirdSet();
                calculateForthSet();
                calculateFifthSet();
            }
        });

        endingWeight.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                setIncrementText();
                calculateFirstTwoSets();
                calculateThirdSet();
                calculateForthSet();
                calculateFifthSet();
            }
        });

        endingWeight.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i == EditorInfo.IME_ACTION_DONE)
                    endingWeight.clearFocus();
                return false;
            }
        });
    }

    private void setPlates() {
        plates.add(1.25);
        plates.add(2.5);
        plates.add(5);
        plates.add(10);
        plates.add(15);
        plates.add(20);
        plates.add(25);
    }

    private void calculateFifthSet() {
        if (!startingWeight.getText().toString().equals("") && !endingWeight.getText().toString().equals("")) {
            float starting = Float.parseFloat(startingWeight.getText().toString());
            float step = calculateIncrement();
            float weight = Math.round(starting + (step * 3), plates);
            fifthSet.setText("5 x " + weight);
        }
    }

    private float roundForPlates(){
        float lightest_plate = plates && plates.size() ? plates.get(plates.size()-1) : 5;
        float atomic_unit = lightest_plate * 2;
        return Math.round(weight/atomic_unit)*atomic_unit;
    }

    private void calculateForthSet() {
        if (!startingWeight.getText().toString().equals("") && !endingWeight.getText().toString().equals("")) {
            float starting = Float.parseFloat(startingWeight.getText().toString());
            float step = calculateIncrement();
            float weight = starting + (step * 2);
            forthSet.setText("5 x " + weight);
        }
    }

    private void setIncrementText() {
        increment.setText("Increment: " + calculateIncrement());
    }

    private void calculateThirdSet() {
        if (!startingWeight.getText().toString().equals("") && !endingWeight.getText().toString().equals("")) {
            float starting = Float.parseFloat(startingWeight.getText().toString());
            float step = calculateIncrement();
            float weight = starting + (step);
            thirdSet.setText("5 x " + weight);
        }
    }

    private void calculateFirstTwoSets() {
        firstSet.setText("5 x "+ startingWeight.getText().toString());
        secondSet.setText("5 x "+ startingWeight.getText().toString());
    }

    //calculates the increment of the single sets an sets it in the textView
    private float calculateIncrement() {
        float incrementValue = 0;
        if (!startingWeight.getText().toString().equals("") && !endingWeight.getText().toString().equals("")) {
            float startWeight = Float.parseFloat(startingWeight.getText().toString());
            float endWeight = Float.parseFloat(endingWeight.getText().toString());
            incrementValue = (endWeight - startWeight) / 4;
        }
        return incrementValue;
    }

    private void instantiateWeights() {
        startingWeight = (EditText) findViewById(R.id.et_starting_weight);
        endingWeight = (EditText) findViewById(R.id.et_end_weight);
    }

    private void instantiateSetStrings() {
        firstSet = (TextView) findViewById(R.id.tv_first_set);
        secondSet = (TextView) findViewById(R.id.tv_second_set);
        thirdSet = (TextView) findViewById(R.id.tv_third_set);
        forthSet = (TextView) findViewById(R.id.tv_forth_set);
        fifthSet = (TextView) findViewById(R.id.tv_fifth_set);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
